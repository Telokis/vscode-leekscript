# leekscript README

This extension is meant to provide syntax highlighting and intellisense for leekwars script files inside [Visual Studio Code](https://code.visualstudio.com/).

## How to install

You need to have [Visual Studio Code](https://code.visualstudio.com/) version 1.17.0 minimum.
You can the go in your extensions and look for `leekscript` and press the "install" button.

## Features

### Since **0.1.0**:
- All constants are recognized
- All builtin functions are recognized
- Basic keyword and operator support

## Preview

![Preview of default file](./images/Example1.png)

## Release Notes

You can find the complete changelog of this extension [here](CHANGELOG.md)
